# AdminApp

This application uses angular 4 created for learning purpose. It has been build using [Angular CLI](https://github.com/angular/angular-cli) version 1.2.3. This app is available in en/nl/fr languages.

## Download dependencies or clone the repository

Run `git clone https://bitbucket.org/ramanujnetherlands/ng4adminapp.git` to clone the repository

Run `npm install` for a AdminApp folder. This will generate all the required library used for the project. All library are mentioned in package.json file with version number.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Run

Run `npm run` to get all available command in the project. This will list out all the available command to run the project so no need to remember each command.

## Build

Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `npm run test-unit` to execute the unit tests

## Running end-to-end tests

Run `npm run test-e2e` to execute the end-to-end tests.

## Running linting for code

Run `npm run lint` to execute the linting for ts files.

## How design looks like?

1: Home page - Login with registered credentials.

![Home page](https://bytebucket.org/ramanujnetherlands/ng4adminapp/raw/3340adfe73f5db21e6a1c3191c01d6068b3230ae/src/assets/images/Design.PNG)

2: Register page - Signup for further login. Details will be saved in local storage

![Register page](https://bytebucket.org/ramanujnetherlands/ng4adminapp/raw/901e37fe10b3a3f4ca3adb621706501e9150bf93/src/assets/images/DesignRegister.PNG)

3: Employee information page - Update employee informations
![Employee information page](https://bytebucket.org/ramanujnetherlands/ng4adminapp/raw/901e37fe10b3a3f4ca3adb621706501e9150bf93/src/assets/images/employeeInfo.PNG)
